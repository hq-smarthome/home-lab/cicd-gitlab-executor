FROM registry.gitlab.com/hq-smarthome/home-lab/docker-base/proxima-ubuntu-base:latest
LABEL service=gitlab-executor

ARG DEBIAN_FRONTEND=noninteractive

# Install Nomad, Terraform, Consul, and vault
RUN curl -fsSL https://apt.releases.hashicorp.com/gpg | apt-key add -
RUN apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
RUN apt-get update && apt-get install -y nomad terraform consul vault packer waypoint && apt-get clean
RUN setcap cap_ipc_lock= /usr/bin/vault

# Install Levant
RUN apt-get update && apt-get install -y curl unzip && apt-get clean
RUN curl -L -o /tmp/levant.zip https://releases.hashicorp.com/levant/0.3.0/levant_0.3.0_linux_amd64.zip && \
    unzip /tmp/levant.zip -d /usr/local/bin/ && \
    rm /tmp/levant.zip
RUN chmod +x /usr/local/bin/levant

# Install jq
RUN apt-get update && apt-get install -y jq && apt-get clean

# Install gitlab runner
RUN apt-get update -y && \
    apt-get install -y --no-install-recommends \
        apt-transport-https \
        git \
        git-lfs \
        dumb-init \
    && apt-get clean

RUN curl -L -o /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64"
RUN chmod +x /usr/local/bin/gitlab-runner

RUN useradd --comment 'GitLab Executor' --no-create-home gitlab-executor --shell /bin/bash
RUN mkdir -p /workspace/build && \
    mkdir -p /workspace/cache && \
    mkdir -p /workspace/tmp && \
    mkdir -p /workspace/scripts && \
    chown --recursive gitlab-executor:gitlab-executor /workspace

COPY --chown=gitlab-executor:gitlab-executor ./scripts /workspace/scripts 
RUN chmod +x /workspace/scripts/*.sh

COPY ./entrypoints /entrypoints 
RUN chmod +x /entrypoints/*.sh

STOPSIGNAL SIGTERM

ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD ["bash", "-c", "/entrypoints/executor.sh"]
