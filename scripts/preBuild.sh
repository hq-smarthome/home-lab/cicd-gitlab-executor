#!/usr/bin/env bash

set -ue

CYAN='\033[0;36m'
NC='\033[0m'

echo "${CYAN}[pre-build]${NC} Runner Script"
